;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 15:49:58 2007
;;
;; This module provides tag support so that users can lookup functions
;; and variables using etags.
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-tags)
(eval-when-compile (require 'etags))



;; --------------- Customizable Variables ---------------

(defcustom nide-tags-program "etags"
  "*Override this if you'd like to run a specific version of etags on your machine.")

(defcustom nide-tags-default-file "TAGS"
  "*Tag filename to use in the event that one is not specified by the project definition file.")

(defcustom nide-tags-auto-generate nil
  "*Generate tags when a project file list is first created.
This may not be desirable since tag creation may take a significant
period of time, especially if the project file list has not been
parsed down enough during the first stages of project definition.
Often, a more efficient approach is to parse down the project files
list and then manually generate the tags via \\[nide-tags-generate].")




;; --------------- Module Local Variables ---------------

(defvar nide-tags-computed-command nil)
(defvar nide-tags-long-command nil)
(defvar nide-tags-e-command nil)




;; --------------- API Functions ---------------

;; Macro to find next tag
(fset 'find-next-tag
   "\C-u\M-.")

(defun nide-tags-generate ()
  (interactive)
  (nide-project-open-assert 'open)
  (message "Generating tags...")
  (let (proc start end default-directory)
    (save-excursion
      (setq default-directory nide-project-dir)
      (or nide-tags-computed-command (nide-tags-generate-defaults))
      (setq proc (cond ((and nide-tags-long-command nide-tags-e-command)
			(start-process "tag-gen" "*Messages*" nide-tags-program "-e" "-o" nide-project-etags-file "-L" "-"))
		       ((and nide-tags-long-command (not nide-tags-e-command))
			(start-process "tag-gen" "*Messages*" nide-tags-program "-o" nide-project-etags-file "-L" "-"))
		       ((and (not nide-tags-long-command) nide-tags-e-command)
			(start-process "tag-gen" "*Messages*" nide-tags-program "-o" nide-project-etags-file "-"))
		       ((and (not nide-tags-long-command) (not nide-tags-e-command))
			(start-process "tag-gen" "*Messages*" nide-tags-program "-o" nide-project-etags-file "-"))
		       (t
			;; Default invocation
			(message "Cannot properly determine correct etag arguments...")
			(start-process "tag-gen" "*Messages*" nide-tags-program "-o" nide-project-etags-file "-"))))
      (if proc
	  (progn
	    (set-process-sentinel proc 'nide-tags-sentinel)

	    ;; Get and send the project file list
	    (set-buffer nide-project-buffer)
	    (setq start (nide-files-get-start))
	    (setq end (nide-files-get-end))

	    (process-send-region proc start end)
	    (process-send-eof proc)
	    t)
	nil))))

(defun nide-tags-generate-empty-file (filename)
  (save-excursion
    (let (buffer full-filename)
      (setq buffer (get-buffer-create filename))
      (set-buffer buffer)
      (erase-buffer)
      (goto-char (point-min))
      (insert ?\f "\n")
      (setq full-filename (concat nide-project-dir filename))
      (write-file full-filename))))

(defun nide-tags-generate-defaults ()
  (save-excursion
    (let (buffer)
      (setq buffer (get-buffer-create " etags-output"))
      (set-buffer buffer)
      (call-process nide-tags-program
		    nil
		    buffer
		    nil
		    "--help")
      (goto-char (point-min))
      ;; Don't ignore case when we search
      (setq case-fold-search nil)
      (if (re-search-forward "\\s-+-L\\s-+" nil t)
	  (setq nide-tags-long-command t)
	(setq nide-tags-long-command nil))
      (goto-char (point-min))
      (if (re-search-forward "\\s-+-e\\s-+" nil t)
	  (setq nide-tags-e-command t)
	(setq nide-tags-e-command nil))
      (kill-buffer buffer)
      (setq nide-tags-computed-command t))))

(defun nide-tags-list-helper (string)
  (goto-char 1)
  (while (re-search-forward string nil t)
    (beginning-of-line)
    (let ((tag (buffer-substring (point)
				 (progn (skip-chars-forward "^\177")
					(point))))
          (props `(action find-tag-other-window mouse-face highlight
			  face ,tags-tag-face))
          (pt (with-current-buffer standard-output (point))))
      (princ tag)
      (when (= (aref tag 0) ?\() (princ " ...)"))
      (add-text-properties pt (with-current-buffer standard-output (point))
                           `(item ,tag ,@props) standard-output))
    (terpri)
    (forward-line 1))
  (when tags-apropos-verbose (princ "\n")))

(defun nide-tags-list (tag)
  "Produce a list of occurances of a selected tag from the project tags file."
  (interactive (nide-project-input-default-tag))
  (save-excursion
    (set-buffer (find-file-noselect nide-project-etags-file t))
    (with-output-to-temp-buffer "*Tags List*")
    (nide-tags-list-helper tag)))

(defun nide-tags-sentinel (process event)
  (if (equal (process-name process) "tag-gen")
      (message "Tag generation complete: %s" event)))
