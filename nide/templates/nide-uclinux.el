;; ucLinux project template module
;; Functions specific to ucLinux projects
;; 
;; Author:       Nathan Crapo
;; Date:         7-28-04
;; Version:      0.2

(provide 'nide-uclinux)

;; User configurable variables.  Customize these after you load the module.
;; ===-- These variables are meant to be used internally --===

(defvar uclinux-project-manager-function nil
  "Interface into the project manager if one exists.  This is for internal use only.")

(defvar uclinux-module-name "ucLinux"
  "The name of this project template module.  This will be used in project template/project manager IPC for the most part.")

;; ===-- Helper functions --===

;; IPC for this project template and a project manager

(defun uclinux-project-manager-request (type &optional ARG)
  "Generic interface function for project manager IPC."
  (if uclinux-project-manager-function
      (funcall uclinux-project-manager-function uclinux-module-name type ARG)
    nil))
 
(defun uclinux-api-func (module-name request &optional ARG)
  "This function allows other modules to get information from this project template."
  ;; Handler for IPC requests from the project manager
  (cond (t
	  nil)))

(defun uclinux-connect-project-manager (f)
  "Connect to a project manager by passing in our interace and receiving an interface back.
The project manager interface is stored away for use by other IPC functions.  Make requests with
sw-project-manager-request."
  (setq uclinux-project-manager-function (funcall f uclinux-module-name 'uclinux-api-func)))


;; --------------- Start ---------------

;; Connect to NIDE if possible
(if (fboundp 'nide-templates-connect)
    (uclinux-connect-project-manager 'nide-templates-connect))
