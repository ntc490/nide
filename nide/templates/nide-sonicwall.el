;; SonicWALL project template module
;; Functions specific to SonicWALL Firewall code.  This module is much more
;; useful when used in conjunction with NIDE (Nathan's Integrated Development
;; Environment).
;; 
;; Author:       Nathan Crapo
;; Date:         7-11-04
;; Version:      0.3

;; 8-9-04:
;; D - Fix filename parsing function for compile so it doesn't affect grep commands.
;; D - Add ROM copy command.
;; D - Fix build select function
;; 
;; 8-7-04:
;; D - Fix build clean commands so they don't remove more than they are supposed to.
;; D - Fix build options so they function using the correct logic (not inverse) and can really turn off options that are set to 1.
;; 
;; 8-4-04:
;; D - Add emtpy sign, ignore dsa, and crazy sign build options
;; D - Change public variables to defcustom and document their usage
;; 
;; 8-2-03:
;; D - Add auto-detect code for target selection
;; D - Fixed m2 build directory selection - was calling build from project directory
;;     instead of buid dir
;; 
;; 7-30-04 ideas:
;; - add sonicwall header creation functions that can be added to the template menu.
;; 
;; 7-27-04 ideas:
;; D - need function to copy target to c:\home when desirable
;; - need function to auto ediff changelists generated with Scott's python script.
;; - ROM dump function
;; - firmware dump function


(provide 'nide-sonicwall)


;; User configurable variables.  Customize these after you load the module.
(defcustom sw-device-mac "0x00, 0x40, 0x10, 0x00, 0x00, 0x00"
  "*The default MAC address for firewall targets.")

(defcustom sw-device-ip "10.1.XX.XX"
  "*The device under test's IP address.
This value gets added to romDefines.h in the proper place.")

(defcustom sw-ftp-server-ip "10.1.XX.XX"
  "*The developer's FTP server.
To be used when running network debug firmware.  This value gets added to romDefines.h in the proper place.")

(defcustom sw-ftp-username "rasgumby"
  "*Username for the developer's FTP server.
Leave as the default for no modification.  Override for more security.")

(defcustom sw-ftp-password "rasgumby"
  "*The password for the developer's FTP server.
Leave the default for no modifications.  Override for more security.")

(defcustom sw-workspace-container "~/"
  "*The container directory is where all the workspaces exist as a general rule.
This is often where the clientspec points in a SonicWALL project.")

(defcustom sw-romdefs-auto-save t
  "*Override this variable to automatically save romDefines.h after it has been modified.")

(defcustom sw-update-frame-title-with-workspace-name nil
  "*Set this to nil if you don't want the frame title played with every time the workspace is changed.")

(defcustom sw-favorite-romdef-alist nil
  "A small list of common PRODCODE macros that the developer uses.
Could be set in emacs startup file.")

(defcustom sw-favorite-target-alist nil
  "A small list of favorite build targets.")

(defcustom sw-project-file-parser-firmware "firmware/fw/system/.*[ch]$\\|firmware/fw/config/.*[ch]$\\|firmware/fw/fwCore/.*[ch]$\\|firmware/h/\\|h/"
  "Custom filter for SonicWALL project file lists.")

(defcustom sw-build-with-deps t
  "*Build with dependency checking enabled."
  :type 'boolean)

(defcustom sw-build-verbose nil
  "*Build with verbose make output."
  :type 'boolean)

(defcustom sw-build-crazy-sign nil
  "*Sign target with developer version of header definition file (hdrdefs.dev)"
  :type 'boolean)

(defcustom sw-build-null-sign nil
  "*Sign target with an empty signature.
These targets can only be loaded using a ROM or firmware image built with the IGNORE_DSA setting enabled."
  :type 'boolean)

(defcustom sw-build-ignore-dsa nil
  "*DEVELOPER USE ONLY!  This setting switches out the DSA checking code in the current target.
If an image built with this option gets out, it will completely defeat SonicWALL's firmware
enforcement paradigm."
  :type 'boolean)


;; ===-- These variables are meant to be used internally --===

(defvar sw-full-romdef-alist nil
  "A complete list of all the PRODCODE macros in romDefines.h.
These values are used for completion during user interaction.")

(defvar sw-full-romdef-alist-exists nil
  "Use this flag to keep track of romdef parsing.")

(defvar sw-project-manager-function nil
  "Interface into the project manager if one exists.  This is for internal use only.")

(defvar sw-workspace-change-hook nil
  "This hook is called when a change is made to the workspace.")

(defvar sw-workspace-directory nil
  "The top level of a SonicWALL code tree.  An example is /somepath/ENH/tip.")

(defvar sw-module-name "SonicWALL"
  "The name of this project template module.  This will be used in project template/project manager IPC for the most part.")

(defvar sw-generated-target-alist nil)

(defvar sw-generated-target-alist-exists nil
  "*Bool value to keep track if the target list needs to be generated for this project.")

(defvar sw-target-selection-history nil)

(defvar sw-prodcode-history nil)

(defvar sw-compilation-parse-errors-filename-function nil)

(defvar sw-invalid-bsp-names (list "vxmake" "ppc" "shared"))

(defvar sw-bsp-history nil)

(defvar sw-clean-history nil)

(defvar sw-template-menu (make-sparse-keymap "SonicWALL"))

(defvar sw-build-options-menu nil)

(defvar sw-compilation-parse-errros-filename-function-old nil)

;; ===-- Helper functions --===

(defun sw-scan-romdef ()
  "Parse through the romDefines.h file and pull out all the PRODCODE_ macros.
This function updates the sw-full-romdef-alist variable and should only be used
internally for this module."
  (save-excursion
    (setq sw-full-romdef-alist ())
    (goto-char (point-min))
    (while (re-search-forward "^#define[ \t]+\\(PRODCODE_[a-zA-Z0-9_]+\\)[ \t]+" nil t)
      ;; Push the match on to the list to return
      (setq sw-full-romdef-alist
	    (append (list (cons (match-string 1) 1)) sw-full-romdef-alist)))))

(defun sw-get-romdefines-buffer ()
  "Look through known directories to find a romdefines file and open it in a new buffer."
  (let ((buffer nil) project-dir filename)
    ;; Try getting the project directory from the project manager
    (setq project-dir (sw-project-manager-request 'get-project-dir))
    (if project-dir
	;; Project manager provided a location
	(setq filename (concat project-dir "/h/romDefines.h"))
      ;; No project manager - find manually
      (let (prompt (override-workspace-dir t))
	;; If there is already a workspace, see if it should be changed before
	;; using it.
	(if sw-workspace-directory
	    (progn
	      (setq prompt (format "Would you like to use \"%s\" as a workspace?: "
				   sw-workspace-directory))
	      (setq override-workspace-dir (not (y-or-n-p prompt)))))
	(if override-workspace-dir
	    (progn
	      (cd sw-workspace-container)
	      (call-interactively 'sw-find-workspace)))
	(setq filename (concat sw-workspace-directory "/h/romDefines.h"))))
    (setq buffer (find-file-noselect filename t))
    (if (eq buffer nil)
	(error "Cannot find a valid SonicWALL romDefines.h file"))
  buffer))

(defun sw-get-user-romdefines-buffer ()
  "Look through known directories to find a user romdefines file and open it in a new buffer.  If one can't be found simply return nil."
  (let ((buffer nil) project-dir filename)
    ;; Try getting the project directory from the project manager
    (setq project-dir (sw-project-manager-request 'get-project-dir))
    (if project-dir
	(progn
	  ;; Project manager provided a location
	  (setq filename (concat project-dir "/h/usrRomDefines.h"))
	  (and (file-readable-p filename)
	      (setq buffer (find-file-noselect filename t)))))
    buffer))


;; IPC for this project template and a project manager

(defun sw-project-manager-request (type &optional ARG)
  "Generic interface function for project manager IPC."
  (if sw-project-manager-function
      (funcall sw-project-manager-function sw-module-name type ARG)
    nil))
 
(defun sw-get-target-list ()
  ;; Enumerate the possible targets if requested
  (if (not sw-generated-target-alist-exists)
      (let (build-dir buffer default-directory)
	(message "Making target list...")
	(setq build-dir (sw-project-manager-request 'get-build-dir))
	(if build-dir
	    (setq default-directory build-dir))
	(setq buffer (get-buffer-create (make-temp-name "make-output")))
	;; Older m2 build processes do not have 'show-target' - detect this
	;; and compensate automatically
	(if (call-process "make" nil buffer nil "show-targets")
	    (call-process "make" nil buffer))
	(save-excursion
	  (set-buffer buffer)
	  (goto-char (point-min))
	  (while (re-search-forward "^\. \\(.*\\)$" nil t)
	    (setq sw-generated-target-alist
		  (append (list (cons (match-string 1) 1)) sw-generated-target-alist))))
	(kill-buffer buffer)
	(setq sw-generated-target-alist-exists t)))
  sw-generated-target-alist)


(defun sw-get-target-subdir (target-name)
  (save-excursion
    (let (buffer default-directory regex (subdir nil))
      ;; Create an empty buffer to put find results in
      (setq buffer (get-buffer-create (make-temp-name " find-target")))
      (set-buffer buffer)

      ;; Set the default directory to our build directory (m2) so the find
      ;; will start from there
      (setq default-directory (sw-project-manager-request 'get-build-dir))

      ;; Find all children directories of our build directory
      (call-process "find" nil buffer nil "-type" "d")

      ;; Do a regular expression search through the results for the
      ;; target name passed in to the function.  Grab the path from
      ;; our build directory to the target name
      (goto-char (point-min))
      (setq regex (format "^\\./\\(.*/\\)%s" target-name))
      (if (re-search-forward regex nil t)
	  (setq subdir (match-string 1)))

      ;; Cleanup the buffer we created to prevent leaks
      (kill-buffer buffer)

      ;; Return the parsed path
      subdir)))
      
(defun sw-get-build-target ()
  (let (cmd)
    (replace-regexp-in-string
     "^make[ ]*" "" 
     (if (setq cmd (sw-project-manager-request 'get-build-command))
	 cmd
       ""))))
			
(defun sw-clean-local ()
  (save-excursion
    (let (build-dir target target-dir default-directory)
      (set-buffer (get-buffer-create "*compilation*"))
      (setq build-dir (sw-project-manager-request 'get-build-dir))
      (setq default-directory build-dir)
      (setq target (sw-get-build-target))
      (if (setq target-dir (sw-get-target-subdir target))
	  (progn
	    (setq default-directory (concat build-dir target-dir target "/"))
	    (compile "make clean"))
	(message "Target does not exist")
	(sit-for 3)))))

(defun sw-clobber-local ()
  (save-excursion
    (let (build-dir target target-dir default-directory)
      (set-buffer (get-buffer-create "*compilation*"))
      (setq build-dir (sw-project-manager-request 'get-build-dir))
      (setq default-directory build-dir)
      (setq target (sw-get-build-target))
      (if (setq target-dir (sw-get-target-subdir target))
	  (progn
	    (setq default-directory (concat build-dir target-dir target "/"))
	    (compile "make clobber"))))))

(defun sw-clean-global ()
  (save-excursion
    (let (build-dir default-directory)
      (set-buffer (get-buffer-create "*compilation*"))
      (setq build-dir (sw-project-manager-request 'get-build-dir))
      (if build-dir
	  (setq default-directory build-dir))
      (compile "make clean"))))

(defun sw-clobber-global ()
  (save-excursion
    (let (build-dir default-directory)
      (set-buffer (get-buffer-create "*compilation*"))
      (setq build-dir (sw-project-manager-request 'get-build-dir))
      (if build-dir
	  (setq default-directory build-dir))
      (compile "make clobber"))))

(defun sw-spotless-global ()
  (save-excursion
    (let (build-dir default-directory)
      (set-buffer (get-buffer-create "*compilation*"))
      (setq build-dir (sw-project-manager-request 'get-build-dir))
      (if build-dir
	  (setq default-directory build-dir))
      (compile "make spotless"))))


(defun sw-clean-build ()
  (interactive)
  (let (target)
    (setq target (completing-read "Type of clean: "
				  '(("spotless"          . 1)
				    ("clobber"           . 1)
				    ("global"            . 1)
				    ("clobber current"   . 1)
				    ("current"           . 1))
				  nil t "current" 'sw-clean-history))
    (cond ((equal target "current")
	   (sw-clean-local))
	  ((equal target "clobber current")
	   (sw-clobber-local))
	  ((equal target "global")
	   (sw-clean-global))
	  ((equal target "clobber")
	   (sw-clobber-global))
	  ((equal target "spotless")
	   (sw-spotless-global))
	  (t
	   nil))))

(defun sw-get-target-cmd (&optional ARG)
  (interactive)
  ;; Get and enumerate the targets by default.  If the command was prefixed,
  ;; use the favorite target list.  Return nil if there was nothing we could
  ;; do and NIDE must do the rest
  (concat "make " (completing-read "Target name: "
				   (if ARG
				       sw-favorite-target-alist
				     (sw-get-target-list))
				   nil nil
				   (sw-get-build-target)
				   'sw-target-selection-history)))

(defun sw-parse-project-files (buffer)
  "Parse down the buffer according to user options if so desired."
  (if (y-or-n-p "Parse down project files from full listing: ")
      (save-excursion
	(set-buffer buffer)
	;; Does the user want BSP files?
	(let (include-bsp platform include-m2 include-rom include-firmware (include-regex-number 0))
	  (if (setq include-bsp (y-or-n-p "Include BSP: "))
	      (setq include-regex-number (+ 1 include-regex-number)))	  

	  ;; Find out what BSP they want if they've already indicated they want BSP
	  (if include-bsp
	      (let (bsp-alist bsp-name)
		(goto-char (point-min))
		(while (re-search-forward "^\./bsp/\\(\\w+\\)/" nil t)
		  (setq bsp-name (match-string 1))
		  (if (and (not (assoc bsp-name bsp-alist))
			   (not (member bsp-name sw-invalid-bsp-names)))
		      ;; Add the bsp to our list
		      (setq bsp-alist (cons (cons bsp-name 1) bsp-alist))))
		(setq platform (completing-read "Select a BSP: " bsp-alist nil nil nil 'sw-bsp-history))))

	  (if (setq include-rom (y-or-n-p "Include ROM files: "))
	      (setq include-regex-number (+ 1 include-regex-number)))
	  (if (setq include-firmware (y-or-n-p "Include select firmware directories: "))
	      (setq include-regex-number (+ 1 include-regex-number)))
	  (if (setq include-m2 (y-or-n-p "Include m2 Makefiles: "))
	      (setq include-regex-number (+ 1 include-regex-number)))

	  ;; Determine the total regex and parse out unwanted lines
	  (let (regex)
	    (goto-char (point-min))
	    (setq regex "^\./\\(")

	    ;; Add BSP to regex
	    (if (and include-bsp (not (equal "" platform)))
		(let (bsp-regex)
		  (setq bsp-regex (format "bspLib.*[ch]$\\|bsp/%s/.*[ch]$\\|bsp/shared/.*[ch]$" platform))
		  (setq regex (concat regex bsp-regex))
		  (if (> include-regex-number 1)
		      (progn
			(setq regex (concat regex "\\|"))
			(setq include-regex-number (- include-regex-number 1))))))

	    ;; Add firmware to regex
	    (if include-firmware
		(progn
		  (setq regex (concat regex sw-project-file-parser-firmware))
		  (if (> include-regex-number 1)
		      (progn
			(setq regex (concat regex "\\|"))
			(setq include-regex-number (- include-regex-number 1))))))

	    ;; Add m2 files to regex
	    (if include-m2
		(progn
		  (setq regex (concat regex "m2/.*\.mak$"))
		  (if (> include-regex-number 1)
		      (progn
			(setq regex (concat regex "\\|"))
			(setq include-regex-number (- include-regex-number 1))))))

	    ;; Add ROM files to regex
	    (if include-rom
		(setq regex (concat regex "rom/.*[ch]$")))
	    (setq regex (concat regex "\\)"))
	    (delete-non-matching-lines regex)))
	t)))

(defun sw-api-func (module-name request &optional ARG)
  "This function allows other modules to get information from this project template."
  ;; Handler for IPC requests from the project manager
  (cond ((eq request 'build-hook)
	 (sw-set-error-file-parse-function))
	((eq request 'open-project)
	 (sw-setup-environment))
	((eq request 'close-project)
	 (setq sw-generated-target-alist nil)
	 (setq sw-generated-target-alist-exists nil)
	 (setq sw-full-romdef-alist nil)
	 (setq sw-full-romdef-alist-exists nil)
	 (sw-unset-error-file-parse-function)
	 (sw-unhook-build-options))
	((eq request 'hook-build-options)
	 (sw-hook-build-options ARG))
	((eq request 'get-build-command)
	 (sw-get-target-cmd ARG))
	((eq request 'parse-project-files)
	 (sw-parse-project-files ARG))
	((eq request 'clean-build)
	 (sw-clean-build))
	((eq request 'add-template-menu)
	 (sw-add-template-menu ARG))
	((eq request 'get-template-data)
	 (sw-get-template-data))
	((eq request 'set-template-data)
	 (sw-set-template-data ARG))
	((eq request 'update-header-defs)
	 (sw-add-template-menu ARG))
	((eq request 'get-project-default-dir)
	 sw-workspace-container)
	((eq request 'generate-build-directory-name)
	 (sw-generate-build-directory ARG))
	((eq request 'use-p4)
	 t)
	(t
	 (message "Unsupported API call to %s from %s" sw-module-name module-name)
	 nil)))

(defun sw-generate-build-directory (project-dir)
  "Generate the name of the build directory from the project directory.
This will depend upon whether the project is using m2 or not.  Since the
naming scheme is uniform and so simple, we shouldn't burden users for it
every time they create a project."
  (let (m2-dir)
    (setq m2-dir (format "%sm2/" project-dir))
    (if (file-directory-p m2-dir)
	m2-dir
      project-dir)))


(defun sw-connect-project-manager (f)
  "Connect to a project manager by passing in our interace and receiving an interface back.
The project manager interface is stored away for use by other IPC functions.  Make requests with
sw-project-manager-request."
  (setq sw-project-manager-function (funcall f sw-module-name 'sw-api-func)))


;;
;; Interactive functions - Users may wish to call these directly or
;; bind them as commands.
;; 

(defun sw-find-workspace(dir)
  (interactive "DWorkspace directory: ")
  (setq sw-workspace-directory dir)
  (if sw-update-frame-title-with-workspace-name
      (set-frame-name dir))
  (cd sw-workspace-directory)
  (run-hooks 'sw-workspace-change-hook))

(defun sw-romdefs(&optional USE-FAVORITES)
  "Modify romDefines.h with useful values.  Use \\[romdefs] to call.
If prefix is used when calling the function, a 'favorite list' is used
as a completion list.  If no prefix is used, a list is generated from
the romDefines.h file itself will be used in completion.
If the file is read-only before the command is run, the buffer will be
marked writable as a side affect.  Since the buffer is not saved after the
modifications occurr, the user is left to save the buffer if they wish."
  (interactive "P")
  (let (romdef-setting-buffer
	romdef-buffer
	user-romdef-buffer
	prodcode prodcode-alist)
    ;; Try and find a romdefines file that is already visited.
    (setq romdef-buffer (get-buffer "romDefines.h"))

    ;; If there isn't one already opened, do some magic to find one.
    ;; Quit with an error if that isn't possible - we can't do anything
    ;; more after that.
    (if (not romdef-buffer)
	(setq romdef-buffer (sw-get-romdefines-buffer)))

    ;; We must have a romdefines.h file.  Error out if there isn't one.
    (if (not romdef-buffer)
	(error "Please visit the romDefines.h file you want to edit first."))

    ;; We need to find a user romdef buffer too if it exists - recent
    ;; feature of m2 build system.
    (setq user-romdef-buffer (sw-get-user-romdefines-buffer))

    ;; Get Prodcode symbol definitions.
    ;; Check to see if we are creating a list of *all* prodcodes or not.
    ;; The user can specify to use favorites with \C-u.  An empty personal
    ;; favorites also leaves us with no option.
    (setq prodcode-alist
	  (if (and USE-FAVORITES
		   (> (length sw-favorite-romdef-alist) 0))
	      ;; Use the favorites
	      sw-favorite-romdef-alist
	    ;; Parse the list 
	    ;; Populate the full romdef alist and prepare to use it.  We
	    ;; store the value in sw-full-romdef-alist so it can be
	    ;; cached
	    (save-excursion
	      (set-buffer romdef-buffer)
	      (sw-scan-romdef)
	      sw-full-romdef-alist)))

    ;; We need to edit different buffers depending upon what is available.
    ;; Some trees have a usrRomDefines.h and some don't.  We prefer a
    ;; usrRomDefines.h, but will fall back to romDefines.h in the event
    ;; that one doesn't exist.
    (setq romdef-setting-buffer
	  (if user-romdef-buffer
	      user-romdef-buffer
	    romdef-buffer))

    ;; Switch the view to the buffer if the user does not care to
    ;; have changes saved automatically.  The user will need to save
    ;; the buffer they are happy with the changes when auto-save is
    ;; not used.
    (if sw-romdefs-auto-save
	(set-buffer romdef-setting-buffer)
      (switch-to-buffer romdef-setting-buffer))

    ;; Leave point and mark alone during manipulations
    (save-excursion
      ;; Make sure the file is not read-only
      (setq buffer-read-only nil)

      ;; User input here.  Use the mouse for a quicker selection
      ;; Prompt the user for the desired prodcode.  Allow completion
      ;; and provide separate history.
      (setq prodcode (completing-read "What product would you like? "
				      prodcode-alist
				      nil nil "PRODCODE_"
				      'sw-prodcode-history))

      ;; Replace the PRODCODE first by locating the ROM_PRODCODE string
      (goto-char (point-min))
      (re-search-forward "\\(ROM_PRODCODE[ \t]+\\)PRODCODE_.*$")
      (replace-match "\\1")
      (insert prodcode)

      ;; Replace the PRODCODE first by locating the ROM_PRODCODE string
      (goto-char (point-min))
      (re-search-forward "\\(ROM_ENET[ \t]+\\)0.*$")
      (replace-match "\\1")
      (insert sw-device-mac)

      ;; Replace the device IP address
      (goto-char (point-min))
      (re-search-forward "\\(FW_IPADDR[ \t]+\"\\).*$")
      (replace-match "\\1")
      (insert sw-device-ip "\"")

      ;; Replace the FTP server IP address
      (goto-char (point-min))
      (re-search-forward "\\(FTP_HOST_IPADDR[ \t]+\"\\).*$")
      (replace-match "\\1")
      (insert sw-ftp-server-ip "\"")

      ;; Replace the FTP username if one is set
      (goto-char (point-min))
      (re-search-forward "\\(FTP_HOST_USERNAME[ \t]+\"\\).*$")
      (replace-match "\\1")
      (insert sw-ftp-username "\"")

      ;; Replace the FTP server IP address
      (goto-char (point-min))
      (re-search-forward "\\(FTP_HOST_PASSWORD[ \t]+\"\\).*$")
      (replace-match "\\1")
      (insert sw-ftp-password "\"")

      ;; Save the buffer if auto-save is desired.
      (if sw-romdefs-auto-save
	  (save-buffer)))))

(defun sw-project-file-parser (filename)
  "Modify the filename when the compile module parses build output so the correct path to the file can be found.
Remove all ../../ characters at the start of the file and replace them with
one instance so the file is referenced to the m2 directory, not the full build
target path."
  (replace-regexp-in-string "^[./]+" "../" filename))

(defun sw-set-error-file-parse-function ()
  (save-excursion
    (let (buffer)
      (set-buffer (get-buffer-create "*compilation*"))
      ;; Only set this variable for the *compilation* buffer so that
      ;; greps still work properly.
      (make-variable-buffer-local 'compilation-parse-errors-filename-function)
      ;; Remember old function value if it exists and hook new function
      ;; for SonicWALL m2 builds
      (setq sw-compilation-parse-errros-filename-function-old compilation-parse-errors-filename-function
	    compilation-parse-errors-filename-function 'sw-project-file-parser))))

(defun sw-unset-error-file-parse-function ()
  (if sw-compilation-parse-errors-filename-function
      (setq compilation-parse-errors-filename-function sw-compilation-parse-errors-filename-function)))

(defun sw-dummy ()
  (interactive)
  (message "Not implemented yet.")
  (sit-for 3))

(defun sw-get-template-data ()
  "Put all project specific settings here so they can be saved"
  (let ((s))
    (if sw-build-verbose
	(setq s (concat s "  <sw-build-verbose/>\n")))
    (if sw-build-with-deps
	(setq s (concat s "  <sw-build-with-deps/>\n")))
    (if sw-build-ignore-dsa
	(setq s (concat s "  <sw-build-ignore-dsa/>\n")))
    (if sw-build-null-sign
	(setq s (concat s "  <sw-build-null-sign/>\n")))
    (if sw-build-crazy-sign
	(setq s (concat s "  <sw-build-crazy-sign/>\n")))
    s))

(defun sw-set-template-data (string)
  "Retrieve settings from the project manager."
  (if (string-match "<sw-build-verbose" string)
      (setq sw-build-verbose t))
  (if (string-match "<sw-build-with-deps" string)
      (setq sw-build-with-deps t))
  (if (string-match "<sw-build-ignore-dsa" string)
      (setq sw-build-ignore-dsa t))
  (if (string-match "<sw-build-null-sign" string)
      (setq sw-build-null-sign t))
  (if (string-match "<sw-build-crazy-sign" string)
      (setq sw-build-crazy-sign t)))

(defun sw-add-template-menu (menu)
  (define-key-after (car menu) [sw-template]
    (list 'menu-item "SonicWALL" sw-template-menu) (cdr menu)))

(defun sw-unhook-build-options ()
  (if sw-build-options-menu
      (progn
	(define-key sw-build-options-menu [toggle-verbose] nil)
	(define-key sw-build-options-menu [toggle-deps] nil)
	(define-key sw-build-options-menu [toggle-ignore-dsa] nil)
	(define-key sw-build-options-menu [toggle-null-sign] nil)
	(define-key sw-build-options-menu [toggle-crazy-sign] nil)
	(setq sw-build-options-menu nil))))

;; Grabbed from menu-bar-make-toggle in menu-bar.el
(defmacro sw-menu-bar-make-toggle (name variable doc message help)
  `(progn
     (defun ,name ()
       ,(concat "Toggle whether to " (downcase (substring help 0 1))
		(substring help 1) ".")
       (interactive)
       (if ,`(setq ,variable (not ,variable))
  	   (message ,message "enabled")
  	 (message ,message "disabled"))
       (sw-setup-environment)
       (sw-project-manager-request 'project-update))
     '(menu-item ,doc ,name
		 :help ,help
                 :button (:toggle . (and (boundp ',variable) ,variable)))))

(defun sw-hook-build-options (menu)
  (setq sw-build-options-menu menu)
  (define-key sw-build-options-menu [toggle-ignore-dsa]
    (sw-menu-bar-make-toggle toggle-ignore-dsa
			     sw-build-ignore-dsa
			     "Ignore DSA"
			     "Firmware DSA checking %s"
			     "Check for DSA signatures during firmware updates"))
  (define-key sw-build-options-menu [toggle-null-sign]
    (sw-menu-bar-make-toggle toggle-null-sign
			     sw-build-null-sign
			     "Empty Sign"
			     "Signing with a NULL signature %s"
			     "Sign the firmware image with a NULL signature"))
  (define-key sw-build-options-menu [toggle-crazy-sign]
    (sw-menu-bar-make-toggle toggle-crazy-sign
			     sw-build-crazy-sign
			     "Crazy Sign"
			     "Build with debug hdrdefs %s"
			     "Sign the firmware image with a developer compatibility list."))
  (define-key sw-build-options-menu [toggle-verbose]
    (sw-menu-bar-make-toggle toggle-verbose
			     sw-build-verbose
			     "Verbose"
			     "Building with verbose output %s"
			     "Build with verbose output."))
  (define-key sw-build-options-menu [toggle-deps]
    (sw-menu-bar-make-toggle toggle-deps
			     sw-build-with-deps
			     "Dependencies"
			     "Building with dependencies mode %s"
			     "Build firmware using dependencies.")))

;; Used to set and remove environment variables according to variable status
(defmacro sw-setenv (variable environment-variable)
  `(if ,variable
       (setq process-environment
	     (cons ,environment-variable process-environment))
     (delete ,environment-variable process-environment)))

(defun sw-setup-environment ()
  "Setup relevant environment varibles for SonicWALL builds"
  (sw-setenv (not sw-build-with-deps) "SW_BUILD_NOT_LOAD_DEPENDENCIES=1")
  (sw-setenv sw-build-crazy-sign      "CRAZY_SIGN=1")
  (sw-setenv sw-build-null-sign       "EMPTY_SIGN=1")
  (sw-setenv sw-build-ignore-dsa      "IGNORE_DSA=1")
  (sw-setenv sw-build-verbose         "SW_BUILD_BE_VERBOSE=1"))

(defun sw-copy-target-home ()
  "Copy the current ROM target to the c:/home directory.
This function is very specific to the author's workspace and should
probably be made more generic."
  (interactive)
  (let (build-dir sub-dir target bootrom source-path dest-path)
    ;; MIPS32 only
    (setq build-dir (sw-project-manager-request 'get-build-dir)
	  target (sw-get-build-target)
	  sub-dir (sw-get-target-subdir target))
    (setq bootrom (if (string-match "mips32" sub-dir)
		      "bootrom.bin"
		    "bootrom"))
    (setq source-path (concat build-dir sub-dir target "/" bootrom))
    (setq dest-path (concat "c:/home/" bootrom))
    (copy-file source-path dest-path t)))

(defun sw-copy-debug-firmware ()
  "Copy the latest debug firmware files (3) to current ftp download
directory."
  (interactive)
  ;; NTC - not finished yet
  (let (build-dir sub-dir source-sub-dir target source-path dest-path)

    ;; Grab the sub-dir of the target
    (setq build-dir (sw-project-manager-request 'get-build-dir))
    (setq target (sw-get-build-target))
    (setq sub-dir (sw-get-target-subdir target))

    ;; The old system uses brecis in favor of mips32.  Do a replace
    (setq source-sub-dir (replace-regexp-in-string "mips32" "brecis" sub-dir))

    ;; Copy vxWorks, vxWorks.sym, and filesys.dat
    (setq source-path (concat build-dir sub-dir target "/vxWorks"))
    (setq dest-path (concat "c:/home/" source-sub-dir "vxWorks"))
    (message "Copying %s to %s" source-path dest-path)
    (copy-file source-path dest-path t)
    (setq source-path (concat build-dir sub-dir target "/vxWorks.sym"))
    (setq dest-path (concat "c:/home/" source-sub-dir "vxWorks.sym"))
    (message "Copying %s to %s" source-path dest-path)
    (copy-file source-path dest-path t)
    (setq source-path (concat build-dir sub-dir target "/filesys.dat"))
    (setq dest-path (concat "c:/home/" source-sub-dir "filesys.dat"))
    (message "Copying %s to %s" source-path dest-path)
    (copy-file source-path dest-path t)))



;; Define the template menu.  This will be added when we register with NIDE
(define-key sw-template-menu [romdef]           '("Edit Romdefs" . sw-romdefs))
(define-key sw-template-menu [ediff-changelist] '("Ediff Changelist" . sw-dummy))
(define-key sw-template-menu [copy-fw]          '("Copy debug firmware to FTP home" . sw-copy-debug-firmware))
(define-key sw-template-menu [copy-rom]         '("Copy ROM target to FTP home" . sw-copy-target-home))


;; --------------- Start ---------------

;; Connect to NIDE if possible
(if (fboundp 'nide-templates-connect)
    (sw-connect-project-manager 'nide-templates-connect))


;;;;; Load and customize Lynn Slater's ducky header code
;;;(defvar make-header-hooks '(
;;;                            header-mode-fill-name
;;;                            header-file-name
;;;                            header-copyright
;;;                            ;;header-sccs
;;;                            ;;header-AFS
;;;                            header-author
;;;                            header-creation-date
;;;                            header-modification-author
;;;                            header-modification-date
;;;                            header-update-count
;;;                            ;;header-status
;;;                            ;;header-blank
;;;                            ;;header-history
;;;                            ;;header-purpose
;;;                            ;;header-toc
;;;                            header-rcs-id
;;;                            ))
;;;
;;;(setq header-copyright-notice
;;;      "Copyright (c)1995-2000 Phobos Corporation.\nAll rights reservered\n\nThis is UNPUBLISHED PROPRIETARY SOURCE CODE of Phobos Corporation;\nthe contents of this file may not be disclosed to third parties, copied or\nduplicated in any form, in whole or in part, without the prior written\npermission of Phobos Corporation;\n\nRESTRICTED RIGHTS LEGEND:\nUse, duplication or disclosure by the Government is subject to restrictions\nas set forth in subdivision (c)(1)(ii) of the Rights in Technical Data\nand Computer Software clause at DFARS 252.227-7013, and/or in similar or\nsuccessor clauses in the FAR, DOD or NASA FAR Supplement. Unpublished -\nrights reserved under the Copyright Laws of the United States.
;;;")
;;;
;;;(defun insert-current-proto-header ()
;;;  "inserts buffer name"
;;;  (interactive)
;;;  (insert "#ifndef _MKPROTO\n#include <")
;;;  (insert (buffer-name))
;;;  (insert ">\n#endif\n")
;;;  )

