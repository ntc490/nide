;; GNU project template module
;; Functions specific to GNU projects
;; 
;; Author:       Nathan Crapo
;; Date:         7-21-04
;; Version:      0.2


(provide 'nide-gnu)


;; User configurable variables.  Customize these after you load the module.
;; ===-- These variables are meant to be used internally --===

(defvar gnu-project-manager-function nil
  "Interface into the project manager if one exists.  This is for internal use only.")

(defvar gnu-module-name "gnu"
  "The name of this project template module.  This will be used in project template/project manager IPC for the most part.")

;; ===-- Helper functions --===

;; IPC for this project template and a project manager

(defun gnu-project-manager-request (type &optional ARG)
  "Generic interface function for project manager IPC."
  (if gnu-project-manager-function
      (funcall gnu-project-manager-function gnu-module-name type ARG)
    nil))
 
(defun gnu-api-func (module-name request &optional ARG)
  "This function allows other modules to get information from this project template."
  ;; Handler for IPC requests from the project manager
  (cond ((eq request nil)
	 (message "test")
	 (sleep-for 2))))

(defun gnu-connect-project-manager (f)
  "Connect to a project manager by passing in our interace and receiving an interface back.
The project manager interface is stored away for use by other IPC functions.  Make requests with
sw-project-manager-request."
  (setq gnu-project-manager-function (funcall f gnu-module-name 'gnu-api-func)))


;; -------------- Start here ---------------

;; Connect to NIDE if possible
(if (fboundp 'nide-templates-connect)
    (gnu-connect-project-manager 'nide-templates-connect))


