;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 15:50:23 2007
;;
;; This module provides menu support for the NIDE module
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-menus)




;; --------------- Module Local Variables ---------------

(defvar nide-top-menu (make-sparse-keymap "NIDE"))
(defvar nide-build-options-menu (make-sparse-keymap "Build Options"))
(defvar nide-project-files-menu (make-sparse-keymap "Project Files"))




;; --------------- NIDE Menu ---------------

;; Functions
(defun nide-build-options-hook-menu ()
  (define-key nide-build-options-menu [build-options-none] nil)
  (nide-templates-request 'hook-build-options nide-build-options-menu))

(defun nide-build-options-unhook-menu ()
  (define-key nide-build-options-menu [build-options-none] '(menu-item "none"
								       nide-dummy
								       :enable nide-enable-build-options)))


;; Key definitions
(define-key nide-project-files-menu [remove-regx] '(menu-item "Remove regexp"
							      nide-files-remove-regexp
							      :enable (nide-project-open-assert 'open t)))
(define-key nide-project-files-menu [add-regx]    '(menu-item "Add regexp"
							      nide-files-add-regexp
							      :enable (nide-project-open-assert 'open t)))
(define-key nide-project-files-menu [del-file]    '(menu-item "Remove current file"
							      nide-files-remove-current
							      :enable (nide-project-open-assert 'open t)))
(define-key nide-project-files-menu [add-file]    '(menu-item "Add current file"
							      nide-files-add-current
							      :enable (nide-project-open-assert 'open t)))
(define-key nide-project-files-menu [list-files]  '(menu-item "List files"
							      nide-files-dir
							      :enable (nide-project-open-assert 'open t)))
(define-key nide-project-files-menu [new-list]    '(menu-item "Create New List"
							      nide-files-create-new-list
							      :enable (nide-project-open-assert 'open t)))

;; This is where build options go.  It's empty for starters.
;; to be done programatically on the fly (late binding)
(define-key nide-build-options-menu [build-options-none] '(menu-item "none"
								     nide-dummy
								     :enable nide-enable-build-options))

;; Top menu main insertion point
(define-key global-map [menu-bar nide]       (cons "NIDE" nide-top-menu))
(define-key nide-top-menu [sep-search]       '("---" . nil))
(define-key nide-top-menu [full-search]      '(menu-item "Full Search" nide-search-full
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [project-search]   '(menu-item "Project Search" nide-search-project-files
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [find-a-file]      '(menu-item "Find a file" nide-search-project-files
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [tag-gen]          '(menu-item "Recalculate tags" nide-tags-generate
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [tag-list]         '(menu-item "Tag List" nide-tags-list))
(define-key nide-top-menu [tag-search-again] '(menu-item "Find tag again" find-next-tag))
(define-key nide-top-menu [tag-search]       '(menu-item "Find tag" find-tag))
(define-key nide-top-menu [sep-build]        '("---" . nil))
(define-key nide-top-menu [prev-error]       '(menu-item "Previous Error" previous-error))
(define-key nide-top-menu [next-error]       '(menu-item "Next Error" next-error))
(define-key nide-top-menu [build-options]
  (list 'menu-item "Build Options" nide-build-options-menu))
(define-key nide-top-menu [clean-build]      '(menu-item "Clean build" nide-build-clean
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [select-build]     '(menu-item "Select build" nide-build-select
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [build]            '(menu-item "Build" nide-build
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [sep-project-2]    '("---" . nil))
(define-key nide-top-menu [project-files]
  (list 'menu-item "Edit Project Files" nide-project-files-menu))
(define-key nide-top-menu [change-cspec]     '(menu-item "Edit Project Clientpsec"
							 nide-project-edit-clientspec
							 :enable (and
								  nide-use-p4
								  (nide-project-open-assert 'open t))))
(define-key nide-top-menu [change-tags]      '(menu-item "Edit Project Tag file"
							 nide-project-edit-tag-file
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [change-bdir]      '(menu-item "Edit Project Build Directory"
							 nide-project-edit-build-dir
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [change-dir]       '(menu-item "Edit Project Directory"
							 nide-project-edit-dir
							 :enable (nide-pdef<-open-assert 'open t)))
(define-key nide-top-menu [change-type]      '(menu-item "Edit Project Type"
							 nide-project-edit-type
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [change-name]      '(menu-item "Edit Project Name"
							 nide-project-edit-title
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [sep-project-1]    '("---" . nil))
(define-key nide-top-menu [close-all-files]  '(menu-item "Close All Project Files"
							 nide-project-ofile-kill-buffers
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [sep-all-files]    '("---" . nil))
(define-key nide-top-menu [close-project]    '(menu-item "Close Project" nide-project-close
							 :enable (nide-project-open-assert 'open t)))
(define-key nide-top-menu [last-project]     '(menu-item "Load Last Project" nide-project-open-last))
(define-key nide-top-menu [load-project]     '(menu-item "Load Project" nide-project-open))
(define-key nide-top-menu [new-project]      '(menu-item "Create Project" nide-project-create))
