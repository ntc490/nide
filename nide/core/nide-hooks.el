;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 15:54:16 2007
;;
;; This module is where NIDE hooks are set
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-hooks)




;; --------------- Init Code ---------------

(add-hook 'kill-emacs-hook 'nide-project-exit)




;; --------------- API Functions ---------------

(defun nide-hooks-find-file-func ()
  (interactive)
  (if (nide-project-open-assert 'open t)
      (progn
	(nide-project-update-last-file)
	(nide-project-save))))
