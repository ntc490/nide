;; AVR-NOS project template module
;; Functions specific to AVR-NOS projects
;; 
;; Author:       Nathan Crapo
;; Date:         9-22-04
;; Version:      0.2


(provide 'nide-avr-nos)


;; User configurable variables.  Customize these after you load the module.
;; ===-- These variables are meant to be used internally --===

(defvar avr-nos-project-manager-function nil
  "Interface into the project manager if one exists.  This is for internal use only.")

(defvar avr-nos-module-name "avr-nos"
  "The name of this project template module.  This will be used in project template/project manager IPC for the most part.")

;; ===-- Helper functions --===

;; IPC for this project template and a project manager

(defun avr-nos-project-manager-request (type &optional ARG)
  "Generic interface function for project manager IPC."
  (if avr-nos-project-manager-function
      (funcall avr-nos-project-manager-function avr-nos-module-name type ARG)
    nil))
 
(defun avr-nos-api-func (module-name request &optional ARG)
  "This function allows other modules to get information from this project template."
  ;; Handler for IPC requests from the project manager
  (cond (t
	  nil)))

(defun avr-nos-connect-project-manager (f)
  "Connect to a project manager by passing in our interace and receiving an interface back.
The project manager interface is stored away for use by other IPC functions.  Make requests with
sw-project-manager-request."
  (setq avr-nos-project-manager-function (funcall f avr-nos-module-name 'avr-nos-api-func)))


;; --------------- Start ---------------

;; Connect to NIDE if possible
(if (fboundp 'nide-templates-connect)
    (avr-nos-connect-project-manager 'nide-templates-connect))
