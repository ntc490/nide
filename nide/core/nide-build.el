;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 15:46:01 2007
;;
;; This module provides build integration
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-build)
(eval-when-compile (require 'compile))




;; --------------- Customizable Variables ---------------

(defcustom nide-default-build-command "make"
  "*User can set the default build command.")

(defcustom nide-use-m2-make nil
  "*Set to TRUE for SonicWALL builds when cygwin make doesn't work.")




;; --------------- Module Local Variables ---------------

(defvar nide-project-build-history nil)
(defvar nide-default-build-clean-command "make clean")




;; --------------- API Functions ---------------

(setq compilation-scroll-output t)


(defun nide-build-select (prefix)
  (interactive "P")
  (nide-project-open-assert 'open)
  (let (command)
  ;; If we can't get something from a project template, fall back to manual
  ;; input from the user.  NIDE should be functional without a template.
  (setq command (nide-templates-request 'get-build-command))
  (if (not command)
      (setq nide-project-build-cmd
	    (read-from-minibuffer "Build Command: "
				  nide-project-build-cmd
				  nil nil
				  'nide-project-build-history))
    (setq nide-project-build-cmd command))
  ;; Update the project with any changes
  (nide-project-update)
  (nide-project-save)))

(defun nide-build-generic-clean (command)
  (interactive (nide-build-input-clean-command))
  (setq nide-default-build-clean-command command)
  (save-excursion
    (let (default-directory)
      (set-buffer (get-buffer-create "*compilation*"))
      (setq default-directory nide-project-build-dir)
      (compile command))))

(defun nide-build-clean ()
  (interactive)
  (nide-project-open-assert 'open)
  (if (not (nide-templates-request 'clean-build))
      (call-interactively 'nide-build-generic-clean)))

(defun nide-build ()
  "Build the current NIDE project."
  (interactive)
  (nide-project-open-assert 'open)
  (let (default-directory buffer)
    (save-excursion
      (setq buffer (get-buffer-create "*compilation*"))
      (set-buffer buffer)
      (setq default-directory nide-project-build-dir)
      (compile nide-project-build-cmd)
      (nide-templates-request 'build-hook))))

(defun nide-build-input-clean-command ()
  (list
   (read-from-minibuffer "Clean command: " nide-default-build-clean-command
			 nil nil 'nide-default-build-clean-command-history)))
