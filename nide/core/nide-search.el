;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 16:05:00 2007
;;
;; This module provides the code to search project files, files under
;; the project directory, etc.
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-search)




;; --------------- Customizable Variables ---------------

(defcustom nide-search-find-program "find"
  "*Name of program used to find files.")




;; --------------- Module Local Variables ---------------

(defvar nide-search-project-files-cmd "xargs grep -n ")
(defcustom nide-search-find-args "-type f"
  "*Default find args - system specific.  You shouldn't have to
change these.")
(defvar nide-search-project-files-history nil)




;; --------------- API Functions ---------------

(defun nide-search-input-search-command ()
  (nide-project-open-assert 'open)
  (list (read-from-minibuffer "Run grep (like this): " nide-search-project-files-cmd nil nil '(nide-search-project-files-history . 1))))

(defun nide-search-full ()
  (interactive)
  (nide-project-open-assert 'open)
  (save-excursion
    (let (default-directory)
      (set-buffer (get-buffer-create "*grep*"))
      (setq default-directory nide-project-dir)
      ;; Works with compile.el module
      (setq grep-find-command (format "find . -type f -printf '\"%%p\"\\n' | grep -v -e \".o$\" -e \"~$\" -e \"%s\" | xargs grep -n " nide-project-etags-file))
      (call-interactively 'grep-find))))

(defun nide-search-find-file ()
  (interactive)
  (nide-project-open-assert 'open)
  (save-excursion
    (let (default-directory)
      (set-buffer (get-buffer-create "*grep*"))
      (setq default-directory nide-project-dir)
      ;; Works with compile.el module
      (setq grep-find-command "find . | grep ")
      (call-interactively 'grep-find))))

(defun nide-search-project-files (command)
  (interactive (nide-search-input-search-command))
  (let (proc buffer files-start null-device
	     files-end default-directory)
    (save-excursion
      ;; Setup for the grep command
      (setq buffer (get-buffer-create "*grep*")) 
      (set-buffer buffer)
      (setq default-directory nide-project-dir)
      (setq null-device nil)

      ;; Leverage the grep command - lots of good stuff here
      (grep command)
      
      ;; Get a pointer to the process
      (setq proc (get-process "grep"))
      (if (not proc)
	  (progn
	    (error "couldn't find grep process!")
	    (sleep-for 2)))

      ;; Get and send the project file list
      (set-buffer nide-project-buffer)
      (setq files-start (nide-files-get-start))
      (setq files-end (nide-files-get-end))
      (process-send-region proc files-start files-end)
      (process-send-eof "grep"))))

