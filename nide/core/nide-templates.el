;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 15:49:16 2007
;;
;; This module provides support to dynamically add template modules to NIDE.
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-templates)




;; --------------- Customizable Variables ---------------

(defcustom nide-templates-import-menus t
  "*Import menus from NIDE templates.
The default is true, but users may wish to supress additional
menus being added to the NIDE parent menu in some cases.")




;; --------------- Module Local Variables ---------------

(defvar nide-templates-alist nil
  "An associative list of all the project template modules that are loaded.")
(defvar nide-templates-project-func nil
  "Function pointer into the project template IPC function.")




;; --------------- IPC function ---------------

;; Typically used for project templates.
(defun nide-templates-api-func (module-name request &optional ARG)
  (cond ((eq request 'get-project-dir)
	 nide-project-dir)
	((eq request 'get-build-dir)
	 nide-project-build-dir)
	((eq request 'get-build-command)
	 nide-project-build-cmd)
	((eq request 'project-update)
	 (if (and (not nide-creating-project)
		  (nide-project-open-assert 'open t))
	     (progn
	       (nide-project-update)
	       (nide-project-save))))
	(t
	 nil)))
	
(defun nide-templates-connect (module-name f)
  ""
  ;; Append the project template if it doesnt already exist
  (if (not (assoc module-name nide-templates-alist))
      (progn
	(setq nide-templates-alist
	      (append (list (cons module-name f)) nide-templates-alist))
	;; Add the template menu if it exists
	(if nide-templates-import-menus
	    (nide-templates-request-with-name module-name 'add-template-menu (cons nide-top-menu  'sep-template)))))
  'nide-templates-api-func)

(defun nide-templates-request-with-name (type cmd &optional ARG)
  "Make a template request to a named project template."
  (let (func)
    (setq func (cdr (assoc type nide-templates-alist)))
    (if func
	(funcall func "nide" cmd ARG)
      nil)))

(defun nide-templates-request (cmd &optional ARG)
  "Make a template request to the current project template.  A project must be open for this to work."
  (if nide-templates-project-func
      (funcall nide-templates-project-func "nide" cmd ARG)
    nil))
