;; -*- Mode: Emacs-Lisp -*-
;;
;; NIDE (Nathan's Integrated Development Environment) project manager.
;; This module allows users to create projects in Emacs so that commonly
;; used settings like frame title, project files, working directory,
;; build commands, Perforce clientspec, and etag file can be quickly
;; recovered from session to session without having to specify or
;; generate them every-time.  This reduces the keystrokes required for
;; common every-day tasks.
;;
;; Several other features that are already provided by other modules
;; have been improved upon by adding a small layer of functionality to
;; their existing capabilities.  No changes are required to the
;; compile or etags modules to be used by NIDE.  They work much more
;; effectively now that they leverages information about the project.
;; As a result, users can build projects with the push of a single
;; button at any time.  When 'next-error and 'previous-error are used,
;; they should always find the file in the project without having to
;; manually point Emacs to files or working directories.  Tag
;; generation is also accomplished from within the Emacs editor so
;; that users don't have to use a separate shell or fancy commands to
;; generate custom lists of tags.
;;
;; Project files is a list of files that comprises the focal point of the
;; current work in the project.  Only the project files are used when
;; generating tags, for example.  The user can also do a grep on the project
;; file list, excluding any other files.  Global grep is also available
;; should the user wish to search everything under the project directory.
;;
;; The NIDE Project Manager will bind dynamically to one or more Project
;; Templates.  Project Templates allow the Project Manager to understand
;; specific characteristics of project types.  Project Templates also
;; provide functions specific to the jobs users may wish to complete when
;; working on that type of project.
;;
;; An example of a Project Template is the nide-sonicwall.el module.
;; This module provides a way for NIDE to determine build targets,
;; create project file lists that only include specific BSP, firmware,
;; or m2 files, find files when using 'next-error after a build, etc.
;; It also provides extra functions like sw-romdefs(), which allows
;; quick modification of romdefines.h without having to visit the file
;; manually or manually parse the prodcode list for the desired ID.
;; Other settings such as MAC address, and IP addresses are also
;; replaced with user-configurable settings.  A few other useful
;; functions also exist and will be added in the future.
;;
;; Multiple Project Templates can be bound to NIDE at one time, but
;; only one Project Template may be active at one time.  Each project
;; has a "type" which determines which template is used while that
;; project is open.  NIDE dynamically chooses the correct template
;; depending upon the setting in the currently assigned project.
;;
;; The SonicWALL module has been the focal point of current design.
;; Other Project Template modules have been also been provided.  New
;; Project Templates can be easily provided and very little Emacs Lisp
;; code is required for basic functionality.
;;
;; Author:             Nathan Crapo
;; Creation Date:      7-19-04
;; Version:            1.0
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide)




;; --------------- Init Code ---------------

(setq load-path (cons "~/lisp/nide/core" load-path))
(setq load-path (cons "~/lisp/nide/templates" load-path))

(eval-when-compile (require 'nide-files))
(eval-when-compile (require 'nide-project))
(eval-when-compile (require 'nide-search))
(eval-when-compile (require 'nide-templates))
(eval-when-compile (require 'nide-build))
(eval-when-compile (require 'nide-hooks))
(eval-when-compile (require 'nide-tags))
(eval-when-compile (require 'nide-style))
(eval-when-compile (require 'nide-misc))
(eval-when-compile (require 'nide-menus))




;; --------------- Global Variables ---------------

(defcustom nide-use-p4 t
  "*Default setting for use of P4 source control system.")

(defcustom nide-p4-favorite-clients nil
  "An alist of favorite p4 clients to use.")




;; --------------- Code Start ---------------

;; Init system wide settings
(setq tags-revert-without-query t)
(setq tags-add-tables nil)




;; --------------- Debug ---------------

;; May some day go away
(defun nide-visit-module ()
  (interactive)
  (find-file "~/lisp/nide.el"))

(defun nide-reload-self ()
  (interactive)
  (load "nide"))

(defun nide-reload-sw ()
  (interactive)
  (load "sonicwall"))

(defun nide-etags-debug ()
  (interactive)
  (nide-project-open-assert 'open)
  (save-excursion
    (set-buffer (get-buffer "*Messages*"))
    (insert "nide-project-etags-file: " nide-project-etags-file "\n")
    (insert "tags-file-name:        " tags-file-name "\n")))

(defun nide-debug-on ()
  "Call this function if you wish debug functionality in NIDE."
  (interactive)
  (defvar nide-debug-menu (make-sparse-keymap "Debug"))
  (define-key nide-debug-menu [reload-sw]        '("load sonicwall" . nide-reload-sw))
  (define-key nide-debug-menu [reload-self]      '("load nide" . nide-reload-self))
  (define-key nide-debug-menu [open-module]      '("visit nide.el" . nide-visit-module))
  (define-key-after nide-top-menu [debug] (list 'menu-item "Debug" nide-debug-menu)))

(defun nide-dummy ()
  (interactive)
  (message "Not implemented yet..."))
