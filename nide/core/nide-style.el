;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 16:09:17 2007
;;
;; This module provides style control.  Things like space vs. tabs,
;; BSD vs. GNU "C" styles, header formats, etc.
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-style)
