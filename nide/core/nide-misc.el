;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 15:45:04 2007
;;
;; This module provides miscellaneous NIDE functionality
;;




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-misc)




;; --------------- API Functions ---------------

(defun nide-misc-nuke-line ()
  "Kill a line, but do not place it in the kill ring."
  (let (start end)
    (setq start (point))
    (end-of-line)
    (setq end (point))
    (delete-region start end)))

(defun nide-misc-get-relative-filename (file)
  (cond ((string-match (concat "^" (regexp-quote (abbreviate-file-name nide-project-dir))) file)
	  (concat "./" (substring file (length (abbreviate-file-name nide-project-dir)) (length file))))
	((string-match (concat "^" (regexp-quote (expand-file-name nide-project-dir))) file)
	 (concat "./" (substring file (length (expand-file-name nide-project-dir)) (length file))))
	(t
	 file)))

(defun nide-misc-get-relative-dir (dir)
  (cond ((string-match (concat "^" (regexp-quote (abbreviate-file-name nide-project-dir))) dir)
	 (concat "./" (substring dir (length (abbreviate-file-name nide-project-dir)) (length dir))))
	((string-match (concat "^" (regexp-quote (expand-file-name nide-project-dir))) dir)
	 (concat "./" (substring dir (length (expand-file-name nide-project-dir)) (length dir))))
	(t
	 dir)))
