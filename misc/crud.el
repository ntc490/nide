(defun for-each-line(func start end)
  "Apply a function to each line in a region.
Uses narror-to-region to isolate each line so the function to
apply to each line doesn't have to deal with crossing line
boundaries."
  (interactive "aFunction: \nr")
  (let (num-lines)
    (setq num-lines (count-lines start end))
    (save-excursion
      (save-restriction
        (narrow-to-region start end)
        (beginning-of-buffer)
        (while (> num-lines 0)
	  (save-restriction
	    ;; Narrow to each line in case we want to insert extra
	    ;; lines and things
	    (narrow-to-region (line-beginning-position) (line-end-position))
	    (funcall func)
	    ;; Moving to the end of the narrowed region ensures that
	    ;; the next line is easier to obtain in the next step
	    (end-of-buffer))
	  (forward-line)   
          (setq num-lines (1- num-lines)))))))

(defun c-chomp()
  "Chomp a line of 'C' code by getting rid of any parens or semi-colons"
  (interactive)
  (save-excursion
    (end-of-line)
    (re-search-backward "\\w[);]" (line-beginning-position))
    (forward-char)
    (kill-line)))

(defun expand_blade_macros()
  (let (property (platform "maple"))
    (c-chomp)
    (beginning-of-line)
    (search-forward (concat platform ", "))
    (delete-region (line-beginning-position) (point))
    (setq property (buffer-string))
    (delete-region (point-min) (point-max))
    (insert (concat "void blade_" platform "_" property "_set(int blade, int state);\n"))
    (insert (concat  "int blade_" platform "_" property "_get(int blade);"))))

(defun dbg-expand()
  "Change printf functions to a macro."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (< (point) (point-max))
      (re-search-forward "printf[ \t]*\(" nil)
      (replace-match "DPRINTF\(\(")
      (re-search-forward "\);")
      (replace-match "\)\);"))))

(defun dbg-collapse()
  "Change debug print macros to printf."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (< (point) (point-max))
      (re-search-forward "NPRINTF[ \t]*\(")
      (replace-match "printf")
      (re-search-forward "\)\);")
      (replace-match "\);"))))

(defun tip()
  "Change the frame name to TIP"
  (interactive)
  (set-frame-name "tip"))

(defun b117()
  "Change the frame name to b117"
  (interactive)
  (set-frame-name "b117"))

;; Take a macro name at the first of the line and turn it in to a printf
(fset 'dump-macro
   [?\C-  ?\C-e ?\C-w tab ?p ?r ?i ?n ?t ?f ?( ?" ?\C-y ?: ?  ?% ?x ?\\ ?n ?" ?, ?  ?\C-y ?) ?\;])

(defun test (start end)
  (interactive "r")
  (save-excursion
    (let (i num-lines)
      (setq num-lines (count-lines start end))
      (goto-char start)

      ;; Delete the characters
      (goto-char start)
      (setq i num-lines)
      (while i
	(delete-char 1)
	(next-line 1)
	(setq i (- i 1)))

      ;; Add the new characters
      (goto-char start)
      (setq i num-lines)
      (while i
	(insert-string "6")
	(next-line 1)
	(setq i (- i 1))))))



(defun convert-file-to-install-line ()
  (interactive)
  (let (current-line path binary command)
    (beginning-of-line)
    (if (re-search-forward "^.*$" nil t)
	(progn
	  (setq current-line (match-string 0))
	  (setq path (file-name-directory current-line))
	  (setq binary (file-name-nondirectory current-line))
	  (setq command (concat "install " binary " " path))
	  (beginning-of-line)
	  (kill-line)
	  (insert command)))))
