;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri May 18 17:11:13 2007
;;
;; This module manages the NIDE project files.  It includes a Project
;; File Browser mode that allows convenient addition and removal of
;; files, quick visits, etc.
;; 




;; --------------- Module Dependencies and Provisions ---------------

(provide 'nide-files)
(require 'nide-misc)




;; --------------- Customizable Variables ---------------

(defcustom nide-files-parse-regexp ".*\\.[cChH]$"
  "*Default project file parsing regular expression.
Used when adding a block of new files.")

(defcustom nide-files-mark-regexp "~$"
  "*Default regular expression for marking files in the project viewer.")

(defcustom nide-files-unmark-regexp "~$"
  "*Default regular expression for unmarking files in the project viewer.")

;; NTC - this could switch off the current mode for more convenience
(defcustom nide-files-add-remove-regexp "\\.\\(c$\\|h$\\)"
  "*Default regular expression for adding and removing project files.")




;; --------------- Module Local Variables ---------------

(defvar nide-files-del-list nil)
(defvar nide-files-browser-mode-on nil)
(defvar nide-files-add-remove-regexp-history nil)
(defvar nide-files-mark-regexp-history nil)
(defvar nide-files-unmark-regexp-history nil)




;; --------------- API Functions ---------------

(defun nide-files-dir ()
  (interactive)
  ;; NTC - make this window the same every time.  Also allow the
  ;; user to jump to a file by pressing enter on the desired line.
  (nide-project-open-assert 'open)
  (let (file files buffer status line cursor)
    (switch-to-buffer (get-buffer-create "*project-files*"))
    (save-excursion
      (setq cursor (point))
      (setq buffer-read-only nil)
      (erase-buffer)
      (insert "  NIDE Project File List:\n")
      (insert "  -----------------------\n")
      (if (not (setq files (nide-files-get-list)))
	  (message "Project is empty")
	(while (setq file (car files))
	  (setq files (cdr files))
	  (if (member file nide-files-del-list)
	      (setq status ?D)
	    (setq status ? ))
	  (setq line (format "%c %s\n" status file))
	  (insert line))))
    (setq buffer-read-only t)
    ;; Save-excursion doesn't seem to work for this - probably
    ;; because of the erase-buffer call, which would move point,
    ;; mark, etc.  Do this instead - should mostly work.
    (goto-char cursor)
    ;; Don't go through the motions of changing modes if it has already
    ;; been done once.
    (if (not nide-files-browser-mode-on)
	(nide-files-browser-mode))))

(defun nide-files-create-new-list ()
  (interactive)
  (nide-project-open-assert 'open)
  (save-excursion
    (let (buffer default-directory)
      (message "Creating project file list...")
      (setq default-directory nide-project-dir)
      (setq buffer (get-buffer-create (make-temp-name " tempfiles")))
      (set-buffer buffer)
      (call-process nide-search-find-program nil buffer nil "-type" "f")
      (message "Completed...")
      ;; Use the template manager to parse down the file list.  If one
      ;; doesn't exist, use the default file parser.
;;      (if (not (nide-templates-request 'parse-project-files buffer))
;;	  (nide-files-default-name-parser buffer))
      (nide-templates-request 'parse-project-files buffer)
      (nide-files-clean-list buffer)
      (nide-files-set-list-with-buffer buffer)
      (nide-project-update)
      (nide-project-save)
      (kill-buffer buffer)))
  ;; If the user is already in the project file mode, revert the buffer
  ;; so the output is updated.
  (if nide-files-browser-mode-on
      (revert-buffer))
  t)



;; User API functions
(defun nide-files-mark ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (let (file)
      (if (and (setq file (nide-files-clean-filename))
	       (not (member file nide-files-del-list)))
	  (progn
	    (setq nide-files-del-list (cons file nide-files-del-list))
	    (setq buffer-read-only nil)
	    (beginning-of-line)
	    (delete-char 1)
	    (insert "D")
	    (backward-char 1)
	    (next-line 1)
	    (setq buffer-read-only t))
	(next-line 1)))))

(defun nide-files-mark-regexp ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (let (file)
      (setq nide-files-mark-regexp
	  (read-from-minibuffer "Mark files by regexp: "
				nide-files-mark-regexp
				nil
				nil
				'nide-files-mark-regexp-history))
      (save-excursion
	(while (re-search-forward nide-files-mark-regexp nil t)
	  (setq file (nide-files-clean-filename))
	  (if file
	      (setq nide-files-del-list (cons file nide-files-del-list))))
	(revert-buffer)))))

(defun nide-files-mark-all ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (setq nide-files-del-list nil)
    (setq nide-files-del-list (nide-files-get-list))
    (revert-buffer)))

(defun nide-files-mark-toggle ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (let (file files (new-del-list nil))
      (setq files (nide-files-get-list))
      (while (setq file (car files))
	(setq files (cdr files))
	(if (not (member file nide-files-del-list))
	    (setq new-del-list (cons file new-del-list))))
      (setq nide-files-del-list new-del-list))
    (revert-buffer)))
  
(defun nide-files-unmark ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (let (file)
      (if (and (setq file (nide-files-clean-filename))
	       (member file nide-files-del-list))
	  (progn
	    (setq nide-files-del-list
		  (delete file nide-files-del-list))
	    (setq buffer-read-only nil)
	    (beginning-of-line)
	    (delete-char 1)
	    (insert " ")
	    (backward-char 1)
	    (next-line 1)
	    (setq buffer-read-only t))
	(next-line 1)))))

(defun nide-files-unmark-all ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (setq nide-files-del-list nil)
    (revert-buffer)))

(defun nide-files-unmark-regexp ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (let (file)
      (setq nide-files-unmark-regexp
	  (read-from-minibuffer "Unmark files by regexp: "
				nide-files-unmark-regexp
				nil
				nil
				'nide-files-unmark-regexp-history))
      (save-excursion
	(while (re-search-forward nide-files-unmark-regexp nil t)
	  (setq file (nide-files-clean-filename))
	  (if (and file
		   (member file nide-files-del-list))
	      (setq nide-files-del-list (delete file nide-files-del-list))))))
    (revert-buffer)))

(defun nide-files-delete ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (if (eq (car nide-files-del-list) nil)
	(message "No files to remove from the project list")
      (let (prompt file)
	(setq prompt (format "Really remove from project list %s" nide-files-del-list))
	(if (not (y-or-n-p prompt))
	    (message "Delete aborted...")
	  (while (setq file (car nide-files-del-list))
	    (setq nide-files-del-list (cdr nide-files-del-list))
	    (nide-files-remove-from-list file))
	  (nide-project-update)
	  (nide-project-save)
	  (nide-files-dir))))))

(defun nide-files-find ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (save-excursion
      (let (file default-directory)
	(setq default-directory nide-project-dir)
	(if (setq file (nide-files-clean-filename))
	    (find-file file)
	  nil)))))

(defun nide-files-view ()
  (interactive)
  (if (not nide-files-browser-mode-on)
      (message "You must be in the project file viewer.")
    (let (file default-directory)
      (setq default-directory nide-project-dir)
      (if (setq file (nide-files-clean-filename))
	  (view-file file)))))

(defun nide-files-add-regexp (recurse)
  (interactive "P")
  (nide-project-open-assert 'open)
  (let (buffer subdir start end file)
    (setq nide-files-add-remove-regexp
	  (read-from-minibuffer "Add files by regexp: "
				nide-files-add-remove-regexp
				nil
				nil
				'nide-files-add-remove-regexp-history))
    (setq subdir (if nide-files-browser-mode-on
		     (nide-files-clean-dirname)
		   (nide-misc-get-relative-dir
		    (file-name-directory
		     (read-file-name "Add directory: "
				     (nide-misc-get-relative-dir default-directory))))))
    (if (or (not (stringp subdir))
	    (equal subdir ""))
	(setq subdir "."))
    (let (default-directory)
      (setq default-directory nide-project-dir)
      (setq buffer (get-buffer-create (make-temp-name " added-files")))
      (if recurse
	  (call-process nide-search-find-program nil buffer nil subdir "-type" "f")
	(call-process nide-search-find-program nil buffer nil subdir "-type" "f" "-maxdepth" "1")))
    (save-excursion
      (set-buffer buffer)
      (goto-char (point-min))
      (delete-non-matching-lines nide-files-add-remove-regexp)
      ;; Move forward through the file to add each file
      (goto-char (point-min))
      (while (< (point) (point-max))
	(beginning-of-line)
	(setq start (point))
	(end-of-line)
	(setq end (point))
	(setq file (buffer-substring start end))
	(if (not (string-match "^\./" file))
	    (setq file (concat "./" file)))
	(nide-files-add-to-list file)
	(forward-char)))
    (kill-buffer buffer))
  (nide-project-update)
  (nide-project-save)
  (if nide-files-browser-mode-on
      (revert-buffer)))

(defun nide-files-remove-regexp (recurse)
  (interactive "P")
  (nide-project-open-assert 'open)
  (let (buffer subdir start end regexp)
    (setq nide-files-add-remove-regexp
	  (read-from-minibuffer "Remove files by regexp: "
				nide-files-add-remove-regexp
				nil
				nil
				'nide-files-add-remove-regexp-history))
    (setq subdir (if nide-files-browser-mode-on
		     (nide-files-clean-dirname)
		   (nide-misc-get-relative-dir
		    (file-name-directory
		     (read-file-name "Remove directory: "
				     (nide-misc-get-relative-dir default-directory))))))
    (if (or (not (stringp subdir))
	    (equal subdir ""))
	(setq subdir "."))
    (save-excursion
      (setq buffer (nide-files-get-list-buffer))
      (set-buffer buffer)
      (goto-char (point-min))
      (if (not recurse)
	  (progn
	    (setq regexp (concat "^\\./" (regexp-quote subdir) "[^/\n]+"))
	    (if (re-search-forward regexp nil t)
		(progn
		  (beginning-of-line)
		  (setq start (point)))
	      (error "Cannot find files in specified directory (%s)" subdir))
	    (setq regexp (concat "^\\./" (regexp-quote subdir) "[^/\n]+/"))
	    (if (or (re-search-forward regexp nil t)
		    (progn
		      (goto-char (point-max))
		      (re-search-backward (concat "^\\./" (regexp-quote subdir) "[^/]+\n") nil t)))
		(progn
		  (end-of-line)
		  (forward-char)
		  (setq end (point)))
	      (error "Cannot find files in specified directory (%s)" subdir))
	    (narrow-to-region start end)))
      (setq regexp (concat "^\\./" (regexp-quote subdir) nide-files-add-remove-regexp))
      (goto-char (point-min))
      (delete-matching-lines regexp)
      (widen)
      (nide-files-set-list-with-buffer buffer)
      (kill-buffer buffer)))
  (nide-project-update)
  (nide-project-save)
  (if nide-files-browser-mode-on
      (revert-buffer)))

;; Project File Navigator support
(defvar nide-files-key-map
  (let ((map (make-sparse-keymap)))
    ;; Key bindings
    (define-key map "\^m" 'nide-files-find)
    (define-key map "v"   'nide-files-view)
    (define-key map "d"   'nide-files-mark)
    (define-key map "u"   'nide-files-unmark)
    (define-key map "x"   'nide-files-delete)
    (define-key map "n"   'next-line)
    (define-key map "j"   'next-line)
    (define-key map "p"   'previous-line)
    (define-key map "k"   'previous-line)
    (define-key map "%a"  'nide-files-add-regexp)
    (define-key map "%r"  'nide-files-remove-regexp)
    (define-key map "g"   'revert-buffer)
    (define-key map "t"   'nide-files-mark-toggle)
    (define-key map "**"  'nide-files-mark-all)
    (define-key map "*!"  'nide-files-unmark-all)
    (define-key map "%u"  'nide-files-unmark-regexp)
    (define-key map "%d"  'nide-files-mark-regexp)

    ;; Mark menu
    (define-key map [menu-bar mark]
      (cons "Mark" (make-sparse-keymap "Mark")))
    (define-key map [menu-bar mark unmark-all]
      '(menu-item "Unmark All" nide-files-unmark-all))
    (define-key map [menu-bar mark mark-all]
      '(menu-item "Mark All" nide-files-mark-all))
    (define-key map [menu-bar mark toggle-mark]
      '(menu-item "Toggle Selection" nide-files-mark-toggle))
    (define-key map [menu-bar mark unmark-regexp]
      '(menu-item "Unmark Regexp" nide-files-unmark-regexp))
    (define-key map [menu-bar mark mark-regexp]
      '(menu-item "Mark Regexp" nide-files-mark-regexp))
    (define-key map [menu-bar mark unmark-file]
      '(menu-item "Unmark File" nide-files-unmark))
    (define-key map [menu-bar mark mark-file]
      '(menu-item "Mark File" nide-files-mark))

    ;; Operate menu
    (define-key map [menu-bar operate]
      (cons "Operate" (make-sparse-keymap "Operate")))
    (define-key map [menu-bar operate remove-files-regexp]
      '(menu-item "Remove files by regexp" nide-files-remove-regexp))
    (define-key map [menu-bar operate add-files-regexp]
      '(menu-item "Add files by regexp" nide-files-add-regexp))
    (define-key map [menu-bar operate sep-add-remove]
      '(menu-item "---" nil))
    (define-key map [menu-bar operate delete-files]
      '(menu-item "Remove Marked files" nide-files-delete))
    (define-key map [menu-bar operate view-file]
      '(menu-item "View file" nide-files-view))
    (define-key map [menu-bar operate find-file]
      '(menu-item "Find file" nide-files-find))
    map)
  "Keymap for NIDE project file lists.")

(defun nide-files-file-in-project-dirp (file)
  "Test if the specified file exists in the project directory or not."
  (if (and (stringp file)
	   (not (equal "" file)))
      (let (short-name)
	(setq short-name (abbreviate-file-name file))
	(if (or (string-match nide-project-dir file)
		(string-match nide-project-dir short-name))
	    t
	  nil))
    nil))
  
(defun nide-files-browser-mode ()
  (interactive)
  (kill-all-local-variables)
  (use-local-map nide-files-key-map)
  (make-local-variable 'revert-buffer-function)
  (make-variable-buffer-local 'revert-buffer-function)
  (make-variable-buffer-local 'nide-files-browser-mode-on)
  (setq nide-files-browser-mode-on t)
  (setq revert-buffer-function 'nide-files-browser-mode-revert)
  ;; NTC - Run hook here
  )
  
(defun nide-files-browser-mode-revert (&optional arg noconfirm)
  "Use this function to revert the project file browser.
This function is required to convert the revert function interface
to something NIDE can deal with."
  (nide-files-dir))

(defun nide-files-clean-list (buffer)
  (save-excursion
    (goto-char (point-min))
    (delete-matching-lines "^\\.$")
    (delete-matching-lines "~$")
    (delete-matching-lines (regexp-quote nide-project-etags-file))))

(defun nide-files-default-name-parser (buffer)
  "Parse down the buffer according to user options if so desired."
  (if (y-or-n-p "Parse down project files from full listing: ")
      (progn
	(save-excursion
	  (setq nide-files-parse-regexp (read-from-minibuffer "File parsing regexp: "
								    nide-files-parse-regexp
								    nil
								    nil
								    'nide-project-file-regexp-history))
	  (set-buffer buffer)
	  (goto-char (point-min))
	  (delete-non-matching-lines nide-files-parse-regexp)
	  t))
    nil))

(defun nide-files-get-list-len ()
  (save-excursion
    (set-buffer nide-project-buffer)
    ;; We don't want to mess up the other functions that access this
    ;; area, so we do another context save.
    (let (start end)
      (setq start (nide-files-get-start))
      (setq end (nide-files-get-end))
      (if (not (and start end))
	  nil
	(count-lines start end)))))

(defun nide-files-get-list-buffer ()
  (let (buffer files start end)
    (save-excursion
      (set-buffer nide-project-buffer)
      (save-excursion
	(setq start (nide-files-get-start))
	(setq end (nide-files-get-end))
	(if (and start end)
	    (progn
	      (setq files (buffer-substring-no-properties start end))
	      (setq buffer (get-buffer-create (make-temp-name " project-files")))
	      (set-buffer buffer)
	      (erase-buffer)
	      (insert files))
	  nil)))
    buffer))
    
(defun nide-files-get-list ()
  "Parse the NIDE project definition file for all project files and return them in a list."
  (save-excursion
    (set-buffer nide-project-buffer)
    (save-excursion
      (let (start file temp-list)
	(setq start (nide-files-get-start))
	(if start
	    (progn
	      (goto-char start)
	      (while (setq file (nide-files-get-next))
		(setq temp-list (cons file temp-list)))
	      (reverse temp-list))
	  nil)))))

(defun nide-files-set-list-with-buffer (buffer)
  (save-excursion
    (set-buffer nide-project-buffer)
    (save-excursion
      (nide-files-clear)
      (goto-char (nide-files-get-start))
      (insert-buffer buffer))))
  
(defun nide-files-add (file)
  (interactive "fFilename: ")
  (nide-project-open-assert 'open)
  (if (not (nide-files-add-to-list (nide-misc-get-relative-filename file)))
      (message "File already exists in project list")
    (nide-project-update)
    (nide-project-save)))

;; NTC
(defun nide-files-list-memberp (file)
  "Test if the specified file exists in the project file list or not."
  (if (and (stringp file)
           (not (equal "" file)))
      (let (short-name)
        (setq short-name (nide-misc-get-relative-filename file))
        (save-excursion
          (set-buffer nide-project-buffer)
          (goto-char (point-min))
          (if (re-search-forward (regexp-quote short-name) nil t)
              t
            nil)))
    nil))

(defun nide-files-memberp (file)
  (save-excursion
    (let (end)
      (set-buffer nide-project-buffer)
      (goto-char (point-min))
      (goto-char (nide-files-get-start))
      (setq end (nide-files-get-end))
      (if (re-search-forward (regexp-quote file) end t)
	  t
	nil))))

(defun nide-files-add-current ()
  (interactive)
  (nide-project-open-assert 'open)
  (let (file)
    (setq file (buffer-file-name (current-buffer)))
    (setq file (nide-misc-get-relative-filename file))
    (if (nide-files-add-to-list file)
	(progn
	  (nide-project-update)
	  (nide-project-save))
      (message "File already exists in project list"))))

(defun nide-files-remove-current ()
  (interactive)
  (nide-project-open-assert 'open)
  (let (file)
    (setq file (buffer-file-name (current-buffer)))
    (setq file (nide-misc-get-relative-filename file))
    (if (not (nide-files-memberp file))
	(message "File doesn't exist in the project list")
      (nide-files-remove-from-list file)
      (nide-project-update)
      (nide-project-save))))

(defun nide-files-find-insertion-point (file start end)
  (let (dir pos)
    (setq dir (file-name-directory file))
    (goto-char start)
    (while (not (equal "" dir))
      (setq pos (re-search-forward (regexp-quote dir) end t))
      ;; If we find the full directory, go to the start and end
      ;; the process by setting dir to an empty string
      (if pos
	  ;; Return the start of the directory listing if it's found
	  (progn
	    (goto-char pos)
	    (beginning-of-line)
	    (setq dir "")))
      ;; Peel off one more level
      (setq dir (replace-regexp-in-string "[^/]*/$" "" dir))
      )))
      
(defun nide-files-add-to-list (file)
  ;; Don't insert the file if it's already there
  (if (nide-files-memberp file)
      nil
    ;; Insert the file at the right point in the list
    (save-excursion
      (set-buffer nide-project-buffer)
      (nide-files-find-insertion-point file
				 (nide-files-get-start)
				 (nide-files-get-end))
      (insert file "\n")
      t)))

(defun nide-files-remove-from-list (file)
  (save-excursion
    (set-buffer nide-project-buffer)
    (save-excursion
      (goto-char (nide-files-get-start))
      (if (re-search-forward (regexp-quote file) nil t)
	  (progn
	    (beginning-of-line)
	    (nide-misc-nuke-line)
	    ;; Remove newline
	    (delete-char 1))))))
    

(defun nide-files-get-next ()
  (if (not (re-search-forward "^\\(.*\\)$" nil t))
      nil)
  (forward-line)
  (if (equal (match-string 1) "</project-files>")
      nil
    (match-string 1)))

(defun nide-files-get-start ()
  (save-excursion
    (set-buffer nide-project-buffer)
    (goto-char (point-min))
    (if (not (re-search-forward "<project-files>" nil t))
	nil
      (forward-line 1)
      (point))))

(defun nide-files-get-end ()
  (save-excursion
    (set-buffer nide-project-buffer)
    (goto-char (point-min))
    (if (not (re-search-forward "</project-files>" nil t))
	nil
      (beginning-of-line)
      (point))))
  
(defun nide-files-clear ()
  (save-excursion
    (set-buffer nide-project-buffer)
    (let (start end)
      (setq start (nide-files-get-start))
      (setq end (nide-files-get-end))
      (if (and start end)
	  (delete-region start end)
	nil))))

;; NTC
(defun nide-files-clean-dirname ()
  (let (dir)
    ;; Return the directory without prefixing ./
    (if (setq dir (nide-files-clean-filename))
	(replace-regexp-in-string "^\\./" "" (file-name-directory dir))
      nil)))

(defun nide-files-clean-filename ()
  (if (> 2 (count-lines 1 (point)))
      nil
    (save-excursion
      (beginning-of-line)
      (if (re-search-forward "^..\\(.*\\)$" nil t)
	  (match-string 1)
	nil))))
  
