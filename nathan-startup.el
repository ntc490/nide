;; -*- Mode: Emacs-Lisp -*-
;; Customized by Nathan Crapo
;; 07-07-2000
;; version 1.4

;; To be used in other modules
(setq user-full-name "Nathan Crapo")

;; Machine dependent override section
(defcustom machdep-forget-p4 nil
  "Use to override loading P4 on a machine level.
Setting this variable will keep P4 from loading regardless of other logic.")
(defcustom machdep-load-p4 nil
  "Machine dependent setting to load P4.  This might be handy on a non-windows box, for example.")
(defcustom machdep-ispell-program nil
  "Machine dependent setting for ispell program
This setting overrides all other settings if defined to something other
than nil.")
(defcustom machdep-override-file ".machdep"
  "*Name of file for machine dependent over-rides.
This can be left alone unless the user wishes to change it for some
reason.")
(defcustom magit-load-path "~/magit"
  "*Location for magit module.  Typically loaded from github via
'git clone https://github.com/magit/magit.git' in the home dir")
(defcustom monky-load-path "~/monky"
  "*Location for monky module.  Typically loaded from github via
'git clone https://github.com/ananthakumaran/monky.git' in the home dir")



;; --------------- Custom Load Path ---------------

(setq load-path (cons "~/lisp/nide" load-path))
(setq load-path (cons "~/lisp/programming" load-path))
(setq load-path (cons "~/lisp/misc" load-path))
(setq load-path (cons magit-load-path load-path))
(setq load-path (cons monky-load-path load-path))
(setq load-path (cons "~/" load-path))




;; --------------- Load modules ---------------

;; Generic section
(load "ntc-lib")
(load "header")
(load "psvn")
(load "code-constructor")
(load "gtags")
(load "white-space")

;; Put the current function in the status bar
;;;(which-function-mode) - This is slooooow

;; Load the machdep file if it exists
(ntc-lib-load-machdep-file machdep-override-file)

;; Python module
(load "python-mode")

;; Python mode hooks
(setq auto-mode-alist
      (append '(("\\.py$" . python-mode)) auto-mode-alist))
(setq interpreter-mode-alist
      (append '(("python" . python-mode)) interpreter-mode-alist))

;; Verilog support
(autoload 'verilog-mode "verilog-mode" "Verilog mode" t)

(setq auto-mode-alist (cons '("\\.v\\'" . verilog-mode) auto-mode-alist))
(add-hook 'verilog-mode-hook '(lambda () (font-lock-mode 1)))

;; Dired module
(require 'dired)
(if (file-exists-p (concat magit-load-path "/magit.el"))
    (require 'magit))
(if (file-exists-p (concat monky-load-path "/monky.el"))
    (require 'monky))
(require 'uniquify)


;; Kill Ring Browser
(require 'browse-kill-ring)
(browse-kill-ring-default-keybindings)

;; NIDE loading happens here.  It doesn't have to be the first module
;; loaded, but Project Templates cannot be connected to NIDE until
;; after it is loaded.
(require 'nide)
(if (and (boundp 'machdep-debug-nide)	; set in machdep.el if needed
	 machdep-debug-nide)
    (nide-debug-on))

;; Load other NIDE project templates too
;(require 'nide-gnu)
;(require 'nide-uclinux)
;(require 'nide-avr-nos)

;; Miscellaneous formatting stuff - not using right now
;(load "ntc-format")


;; ---------------- Perforce Integration ---------------

;; Prevent long delays in file hoooks if the P4 module is loaded
(setq p4-use-p4config-exclusively t)

;; Load Perforce in the right situation.
;; Never load it if machdep-forget-p4 is set true.
(if (not machdep-forget-p4)
    (progn
      ;; Load it if told to do so in the machdep file or if running on
      ;; a Windows box
      (cond (machdep-load-p4
	     (message "Loading P4 module as instructed in our machdep file...")
	     (load "p4"))
	    ((ntc-lib-running-on-windows-p)
	     (message "Loading P4 module - running on a Windows Machine...")
	     (load "p4")))))


;; --------------- Symlink Expansion In Minibuffer ---------------

(defun resolve-sym-link ()
  "Replace the string at the point with the true path."
  (interactive)
  (beginning-of-line)
   (let* ((file (buffer-substring (point)
                                    (save-excursion (end-of-line) (point))))
            (file-dir (file-name-directory file))
    (file-true-dir (file-truename file-dir))
    (file-name (file-name-nondirectory file)))
    (delete-region (point) (save-excursion (end-of-line) (point)))
    (insert (concat file-true-dir file-name))))

(define-key minibuffer-local-completion-map (kbd "C-r") 'resolve-sym-link)



;; --------------- Ispell ---------------

;; Ispell isn't always found that easily on Windows machines
(cond (machdep-ispell-program		; This override takes precedence
       (setq ispell-program-name machdep-ispell-program))
      ((ntc-lib-running-on-windows-p)
       (setq ispell-program-name "c:/cygwin/bin/aspell.exe")))




;; --------------- Generic key binding ---------------

(global-set-key "\M-s"   'gtags-find-symbol)
(global-set-key "\M-,"   'gtags-find-rtag)
(global-set-key "\M-."   'gtags-find-from-here)
(global-set-key "\M-g"   'goto-line)
(global-set-key "\M-r"   'revert-buffer)
(global-set-key "\M-o"   'tags-search)
(global-set-key "\M-/"   'hippie-expand)
;(global-set-key "\M- "   'just-one-space)
(global-set-key [f1]     'nide-project-open)
(global-set-key [f2]     'nide-search-full)
(global-set-key [f3]     'nide-search-project-files)
(global-set-key [f4]     'nide-search-project-files)
(global-set-key [M-f1]   'nide-project-open-last)
(global-set-key [f6]     'nide-tags-generate)
(global-set-key [f7]     'previous-error)
(global-set-key [f8]     'next-error)
(global-set-key [f5]     'nide-build)
(global-set-key [M-f5]   'nide-build-select)
(global-set-key [f12]    'ntc-rename-func)
(global-set-key "\C-ce"  'ediff-buffers)
(global-set-key "\C-xnc" 'nide-build-clean)
;(global-set-key "\C-xnn" 'nide-files-create-new-list)
(global-set-key "\C-xnl" 'nide-files-dir)
(global-set-key "\C-xna" 'nide-files-add-current)
;(global-set-key "\C-xnr" 'nide-files-remove-current)
(global-set-key "\C-c "  'cc-find-other-file)
(global-set-key "\C-ca"  'cc-append-to-line)
(global-set-key "\C-cc"  'cc-chomp-lines)
(global-set-key "\C-cr"  'cc-chomp-lines-regexp)
(global-set-key "\C-cf"  'cc-func-body)
(global-set-key "\C-ci"  'cc-if-clause)
(global-set-key "\C-ch"  'cc-numbers-hex)
(global-set-key "\C-cn"  'cc-numbers)
(global-set-key "\C-ct"  'cc-align-table)
(global-set-key "\C-c^"  'cc-join-lines)
(global-set-key "\C-xg"  'magit-status)
;; \C-x\C-b bound to 'list-buffers by default.  Use prefix for all files.
(global-set-key "\C-x\C-b" 'bs-show)

;; \C-xb bound to 'switch-to-buffer by default
(iswitchb-mode 1)
(setq iswitchb-buffer-ignore '("^ " "^\\*"))

;; Mouse support in the terminal.  Use xterm-mouse-mode interactively to toggle
;; it on and off.  Use negative arg to turn it off, positive arg to turn it on.
;; Note you can't do cut-n-paste when it's on.  Navigation with it on can be
;; nice.  Leave it off by default.
(require 'mouse)
;;(xterm-mouse-mode t)
;;(defun track-mouse (e))

; Index wheel left, right
(global-set-key [mouse-6] 'previous-buffer)
(global-set-key [mouse-7] 'next-buffer)

; Mouse thumb back, forward
(global-set-key [mouse-8] 'find-tag)
(global-set-key [mouse-9] 'newline)
;;(global-set-key [mouse-9] 'undo)

; Mouse Magnifier glass
(global-set-key [XF86Search] 'delete-other-windows)

; Mouse thumb wheel up, down, press
(global-set-key [mouse-13] 'yank)
(global-set-key [mouse-15] 'kill-region)
(global-set-key [mouse-17] 'find-tag)

(define-key emacs-lisp-mode-map "\C-xx" 'edebug-defun)




;; --------------- Global Settings ---------------

(setq-default show-trailing-whitespace t)
(menu-bar-mode 0)
(setq uniquify-buffer-name-style 'reverse)
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)
(setq uniquify-ignore-buffers-re "^\\*")
(setq read-buffer-completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)
(setq backup-inhibited t)
(put 'narrow-to-region 'disabled nil)
(put 'eval-expression 'disabled nil)
(put 'erase-buffer 'disabled nil)
(setq line-number-mode t)
(column-number-mode 'true)
(setq compile-command "make")

;; Beginners might consider leaving this nil
(setq inhibit-startup-message t)

(defun my-c-mode-common-hook ()
  (c-set-style "BSD")
  (gtags-mode 1)
  (whitespace-indent-mode-hook))
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

(setq minibuffer-max-depth nil)
(custom-set-faces
  ;; custom-set-faces was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 )
