;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-Lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author          : Nathan Crapo
;; Created On      : Fri Jun  3 17:08:16 2005
;; 
;; Functions that control features that are only used some of the time.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




(defun ntc-features-time ()
  "Add the time to the status bar.  This is useful not only to tell time, but as a keep-alive for annoying firewalls."
  (interactive)
  (require 'time)
  (display-time))



(defun ntc-features-start-ecb ()
  "Start using Emacs Code Browser.  Don't use very often yet.  Just checking it out.  It takes a long time to load, so don't leave it as a default."
  (ntc-features-setup-ecb-load-path)
  (require 'speedbar)
  (require 'eieio)
  (require 'semantic)
  (require 'ecb))





(defun ntc-features-setup-ecb-load-path ()
  "Setup load path modifications needed to load ECB modules."
  ;;(setq load-path (cons "~/lisp/cedet-1.0beta3b" load-path))
  ;;(setq load-path (cons "~/lisp/cedet-1.0beta3b/cogre" load-path))
  ;;(setq load-path (cons "~/lisp/cedet-1.0beta3b/contrib" load-path))
  ;;(setq load-path (cons "~/lisp/cedet-1.0beta3b/semantic" load-path))
  ;;(setq load-path (cons "~/lisp/cedet-1.0beta3b/eieio" load-path))
  (setq load-path (cons "~/lisp/cedet-1.0beta3b/common" load-path))
  (setq load-path (cons "~/lisp/cedet-1.0beta3b/speedbar" load-path)))
